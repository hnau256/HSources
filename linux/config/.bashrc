#
# ~/.bashrc
#

PATH=$PATH:~/.local/bin

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1="\[\e[1;93m\]\u@\h \[\e[1;94m\][\W] \[\e[1;97m\]\$ \[\e[0m\]"

export PASSWORD_STORE_DIR="/files/pass"
export EDITOR=mcedit
