#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1="\$(if [[ \$? == 0 ]]; then echo \"\[\033[1;32m\]\342\234\223\"; else echo \"\[\033[1;31m\]\342\234\227\"; fi)\[\033[00m\] \[\e[1;37m\][\u \[\e[1;33m\]\w\[\e[1;37m\]]\$ "
trap 'printf "\e[0m" "$_"' DEBUG

HISTSIZE=5000
HISTFILESIZE=10000
shopt -s histappend

alias ls='ls --color=auto'
