#!/bin/bash

sudo pacman -Rns $(pacman -Qtdq)
sudo pacman -Suy --noconfirm

pip-review -a

$(dirname "$0")/press_any_key_to_continue.sh