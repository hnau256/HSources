#!/bin/bash

if [ -z "$1" ]; then
  echo "Error: value not defined"
  exit 1
fi

CPU_COUNT=$(lscpu | grep 'CPU(s):' | head -1 | awk '{print $2}')
for i in $(seq 1 $(expr $CPU_COUNT - 1));
do
   sudo echo $1 | sudo tee  /sys/devices/system/cpu/cpu${i}/online > /dev/null
done