#!/bin/bash

if [ -z "$1" ]; then
  echo "Error: mountpoint net defined"
  read -p "Press any key to continue... " -n1 -s
  exit 1
fi

cd `dirname "$0"`

if  grep -q "lsec" /proc/mounts; then
  echo "Error: container already opened"
else
  sudo cryptsetup luksOpen lsec.crt lsec
  sudo mount /dev/mapper/lsec $1 -o noatime,ro
  echo "Success: container opened"
fi

$(dirname "$0")/../linux/scripts/press_any_key_to_continue.sh
