#!/bin/bash

cd `dirname "$0"`

if  grep -q "lsec" /proc/mounts; then
  sudo umount /dev/mapper/lsec
  sudo cryptsetup luksClose /dev/mapper/lsec
  echo "Success: container closed"
else
  echo "Error: container already closed"
fi

$(dirname "$0")/../linux/scripts/press_any_key_to_continue.sh
