`Проект` - `Community Support - Open Source Project Repository Hosting (OSSRH)`  
`Тип задачи` - `New project`  
`Тема` - `$LIBRARY_UI_NAME`  
`Описание` - Краткое описание библиотеки  
`Group Id` - `$GROUP_ID`  
`Project URL` - URL бибилотеки, например `https://github.com/hnau256/HLog`  
`SCM url` - например `https://github.com/hnau256/HLog.git`  
`Username(s)` - Ник пользователя в `Sonatype`  