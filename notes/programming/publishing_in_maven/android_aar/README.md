# Публикация Android библиотеки в [Maven Central](http://search.maven.org/)

### Переменные
* `GROUP_ID` - идентификатор разработчика
* `LIBRARY_NAME` - название библиотеки
* `LIBRARY_UI_NAME` - Человекочитаемое название библиотеки

### Создание библиотеки на основе проекта в AndroidStudio
* `AndroidStudio -> File -> New -> New project...`
* Переименовать `app` в `$LIBRARY_NAME`
* Удалить тег `<application>` из `./$LIBRARY_NAME/src/main/AndroidManifest.xml`
* Заменить `apply plugin: 'com.android.application'` на `'apply plugin: com.android.library'` в файле `./$LIBRARY_NAME/build.gradle`
* Удалить строку `applicationId "$GROUP_ID.$LIBRARY_NAME"` из файла `./$LIBRARY_NAME/build.gradle`

### Запрос разрешения на публикацию библиотеки
* [Зарегистрироваться](https://issues.sonatype.org/secure/Signup!default.jspa) в `Sonatype`
* Нажать кнопку `Создать` на [странице](https://issues.sonatype.org/browse/OSSRH)
* Заполнить [значения](./sonatype_ossrh_params.md)
* Дождаться одобрения заявки (А можно и выполнять последующие действия, но при нажатии на кнопку `Release` на [странице](https://oss.sonatype.org/index.html#stagingRepositories) будет возращаться ошибка до одобрения заявки)

### `Gradle` скрипт для публикации в `Maven`
* Загрузить в `./` скрипт [maven_push.gradle](./maven_push.gradle)
* В `./gradle.properties` добавить [строки](./gradle.properties.md)
* В `./build.gradle` добавить [строки](./build.gradle)
* Создать файл `./$LIBRARY_NAME/gradle.properties` с [содержимым](./library/gradle.properties.md)
* Добавить в файл `./$LIBRARY_NAME/build.gradle` строку `apply from: '../maven_push.gradle'`

### Генерация ключей для подписи файлов ([дополнительно](./gpg_info.md))
* `gpg --gen-key`
* `gpg --list-secret-keys --keyid-format SHORT` (В выводе можно найти `$GPG_KEY_ID`)
* `gpg --keyserver hkp://keyserver.ubuntu.com --send-keys $GPG_KEY_ID` (Возможные адреса серверов: `hkp://keyserver.ubuntu.com`, `hkp://keyserver.ubuntu.com:80`)
* `gpg --export-secret-key $GPG_KEY_ID >~/.gnupg/secring.gpg`
* Добавить в файл `~/.gradle/gradle.properties` [строки](./user_gradle.properties.md) (или создать новый, если отсутствует)

#### Загрузка на сервер
![AndroidStudio](./images/android_studio_upload.png)

### Публикация библиотеки
* Найти свою библиотеку на [странице](https://oss.sonatype.org/index.html#stagingRepositories)
* На вкладке `content` проверить содержимое
* Нажать кнопку `Close`
* Подождать, пока репозиторий не будет проверен (прогресс виден на вкладке `activity`)
* Нажать кнопку `Release`

#### Используемая литература
* [Публикация Android-библиотеки в репозиторий Maven с помощью Gradle](https://annimon.com/article/1420)
