`signing.keyId`=`$GPG_KEY_ID`  
`signing.password`=пароль от ключа, который вы указывали при создании  
`signing.secretKeyRingFile`=`/home/$USER_NAME/.gnupg/secring.gpg`  
 
`nexusUsername`=Ник пользователя в `Sonatype`  
`nexusPassword`=Пароль пользователя в `Sonatype`